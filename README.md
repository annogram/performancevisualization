# SE751-PerformanceVisualisation

<!-- TOC -->

- [SE751-PerformanceVisualisation](#se751-performancevisualisation)
    - [About](#about)
    - [ParaTracer](#paratracer)
        - [How to instrument your code using ParaTracer](#how-to-instrument-your-code-using-paratracer)
        - [Trace files](#trace-files)
    - [Pajé](#paj)
    - [Team](#team)

<!-- /TOC -->

## About

Parallel programs exist to perform large computational tasks quicker than is possible during the sequential execution of a program. Large tasks can be segmented and delegated to different threads and processes to theoretically cut the runtime from n down to _n/k_,  with _k_ pocessors. 

The ParaTask eclipse plugin allows developers the option to create _High performance_ programs by abstracting away the problems of thread and process management using simple primitives in a .ptjava file.

The problem with creating high performance applications is the added complexity that comes with it, debugging programs can be orders of magnitude more difficult than sequential programs and sometimes impossible using conventional means on distributed systems, this is where program profiling and tracing comes in. Enabling  a program to be traced or profiled is known as **Instrumenting**. Instrumentation is done to find bottlenecks and follow the execution path that a parallel program takes during its lifetime.

Parallel computing is very mature in lanuages such as C/C++ and Fortran and there are many well known methods of instrumentation and many libraries out there that perform instrumentation. Java on one hand does have its own _automated_ profiling and tracing framework from the **Java Mission Control** application that is now bundled with all Java JDKS. However the tracefiles created by this tool can only be used by this tool and is notably lacking in parallel program monitoring.

## ParaTracer
What we provide is the ability for developers to manually add classes that they're interested in into an agent which will instrument the code at runtime. Developers can _create_ class transformers easily by extending our abstract `ClassTransformer` class and overriding the methods to instrument methods they're interested in. S

### How to instrument your code using ParaTracer

1. Make sure you have the package `com.uoa.se751.paraTaskProject.trace` in your project hierarchy and `agent.jar` in your root directory.
2. In eclipse add the line `-javaagent:agent.jar` to your _JVM arguments_
![Run Config](readme1.png)
![JVM arguments](readme2.png)


3. To instrument a class is very simple. You must simply extend the abstract class `com.uoa.se751.paraTaskProject.trace.ClassTransformer` and use the hooks in said class to return the information that we need to instrument.
```Java
package com.uoa.se751.paraTaskProject.trace;

import com.uoa.se751.paraTaskProject.trace.util.MethodContext;

public class SIMDTransformer extends ClassTransformer {

	@Override
	protected String defineClassName() {
		return "com/uoa/se751/paraTaskProject/SIMD";
	}

	@Override
	protected MethodContext[] methodsToInstrument() {
		return new MethodContext[]{new MethodContext("arrayJoiner", false)};
	}
}
```

A short description on whats being done here
* In `defineClassName()` you must define the file path from the root package directory to the class, seperating the folder names with forward slashes **(/)**
* The `methodsToInstrument()` requires you to return the names of all the methods you wish to instrument and a boolean declaring weather or not method is a ParaTask method or not. This is wrapped neatly in a `MethodContext` object.

4. The java agent, `agent.jar`, looks for the `ParaAgent.java` class in the `com.uoa.se751.paraTaskProject.trace` package. `ParaAgent` provides a method for adding transformed classes into the `agent.jar` so that a class's bytecode can be analyzed at runtime. To do this you **MUST** add the class transformer to the instrumented code first thing in the main method. Any classes that you wish to instrument, you must create a transformer and add it to the `globalInstr` using the `addTransformer(ClassTransformer)` command.
```Java
public static void main(String[] args) {
    ParaAgent.globalInstr.addTransformer(new SIMDTransformer());
    // notice that the SIMD class transformer
    //  is added to the globalInstr before it is ever called in the program
    simdTester = new SIMD();
    ...
```

### Trace files


The entire motivation behind transforming our classes is to create tracefiles that have data about the applications running time. We use the Visulisation tool Pajé to display runtime information about programs. This format was chosen because it is the only open source format which doesn't use an external library and is therefore able to be implemented in Java without sideloading C or Fortran code.

These files can be loaded into the Pajé application to show information about the program's runtime, method calls made, length of method execution, and tasks that are running through in each thread.

## Pajé
Installing Pajé visualisation tool:
1.	Be on Linux
2.	Open terminal and run this
```bash
sudo apt-get update
sudo apt-get install paje.app
```
To run Pajé, open terminal and enter:
>	openapp Paje

To run it and open a trace file, enter:
>	openapp Paje “path to trace file”

There are 2 views available in Pajé. A space time diagram, and statistics view.

![](Paje1.PNG)

This shows the timeline of the threads executing in the program. 
* ParaTask threads have sub-containers which show which task the thread is executing
* Altering of the container display can be done via Tools -> Shapes & size… option
* You may click on a specific task or thread to view more detailed about start, end and execution time.

![](Paje2.PNG)

This view shows a pie chart of the task and thread states. It also shows more detailed information, such as the task name of the method which is executing.

![](Paje3.PNG)

## Team

* Akram Darwazeh
* John Ramirez
* Hailun Wang
* Henry Wu