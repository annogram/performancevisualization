package com.uoa.se751.paraTaskProject;//####[1]####
//####[1]####
import java.util.Arrays;//####[3]####
import java.util.Iterator;//####[4]####
import java.util.List;//####[5]####
import java.util.LinkedList;//####[6]####
import java.util.concurrent.ThreadLocalRandom;//####[7]####
import pu.pi.ParIteratorFactory;//####[9]####
//####[9]####
//-- ParaTask related imports//####[9]####
import pt.runtime.*;//####[9]####
import java.util.concurrent.ExecutionException;//####[9]####
import java.util.concurrent.locks.*;//####[9]####
import java.lang.reflect.*;//####[9]####
import pt.runtime.GuiThread;//####[9]####
import java.util.concurrent.BlockingQueue;//####[9]####
import java.util.ArrayList;//####[9]####
import java.util.List;//####[9]####
//####[9]####
/**
 * A class containing functions that fit the Single Instruction Multiple Data
 * type of program.
 * 
 * @author Akram Darwazeh, Hailun Wang and Henry Wu
 *
 *///####[16]####
public class SIMD {//####[17]####
    static{ParaTask.init();}//####[17]####
    /*  ParaTask helper method to access private/protected slots *///####[17]####
    public void __pt__accessPrivateSlot(Method m, Object instance, TaskID arg, Object interResult ) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {//####[17]####
        if (m.getParameterTypes().length == 0)//####[17]####
            m.invoke(instance);//####[17]####
        else if ((m.getParameterTypes().length == 1))//####[17]####
            m.invoke(instance, arg);//####[17]####
        else //####[17]####
            m.invoke(instance, arg, interResult);//####[17]####
    }//####[17]####
//####[19]####
    public int sumTo(int... args) {//####[19]####
        int len = args.length, iterator = 0, sum = 0;//####[20]####
        while (iterator < len - 1) //####[21]####
        {//####[21]####
            sum = args[iterator] + args[iterator + 1];//####[22]####
            iterator++;//####[23]####
        }//####[24]####
        return sum;//####[25]####
    }//####[26]####
//####[37]####
    /**
	 * Makes an expensive hello world operation that does many array joins.
	 * Single Instruction Multiple Data, with the single instruction being the
	 * call to this and the multiple data being mocked up by the arrays.
	 * 
	 * Used for testing arrayJoiner
	 * 
	 * @return <code>String</code> <strong>"Hello World."</strong>
	 *///####[37]####
    public String helloWorldLong() {//####[37]####
        char[][] helloWorldArr = new char[][] { new char[] { 'H' }, new char[] { 'e' }, new char[] { 'l' }, new char[] { 'l' }, new char[] { 'o' }, new char[] { ' ' }, new char[] { 'W' }, new char[] { 'o' }, new char[] { 'r' }, new char[] { 'l' }, new char[] { 'd' }, new char[] { '.' } };//####[38]####
        char[] helloWorld = new char[0];//####[41]####
        for (int i = 0; i < helloWorldArr.length; i++) //####[43]####
        {//####[43]####
            char[] current = helloWorldArr[i];//####[44]####
            char[] temp = Arrays.copyOf(helloWorld, helloWorld.length + current.length);//####[47]####
            for (int j = helloWorld.length; j < temp.length; j++) //####[48]####
            {//####[48]####
                temp[j] = current[helloWorld.length - j];//####[49]####
            }//####[50]####
            helloWorld = temp;//####[51]####
        }//####[52]####
        return new String(helloWorld);//####[54]####
    }//####[55]####
//####[63]####
    private static volatile Method __pt__arrayJoiner_IteratorDoubleArAr_method = null;//####[63]####
    private synchronized static void __pt__arrayJoiner_IteratorDoubleArAr_ensureMethodVarSet() {//####[63]####
        if (__pt__arrayJoiner_IteratorDoubleArAr_method == null) {//####[63]####
            try {//####[63]####
                __pt__arrayJoiner_IteratorDoubleArAr_method = ParaTaskHelper.getDeclaredMethod(new ParaTaskHelper.ClassGetter().getCurrentClass(), "__pt__arrayJoiner", new Class[] {//####[63]####
                    Iterator.class//####[63]####
                });//####[63]####
            } catch (Exception e) {//####[63]####
                e.printStackTrace();//####[63]####
            }//####[63]####
        }//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public TaskIDGroup<Void> arrayJoiner(Iterator<Double[][]> it) {//####[63]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[63]####
        return arrayJoiner(it, new TaskInfo());//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public TaskIDGroup<Void> arrayJoiner(Iterator<Double[][]> it, TaskInfo taskinfo) {//####[63]####
        // ensure Method variable is set//####[63]####
        if (__pt__arrayJoiner_IteratorDoubleArAr_method == null) {//####[63]####
            __pt__arrayJoiner_IteratorDoubleArAr_ensureMethodVarSet();//####[63]####
        }//####[63]####
        taskinfo.setParameters(it);//####[63]####
        taskinfo.setMethod(__pt__arrayJoiner_IteratorDoubleArAr_method);//####[63]####
        taskinfo.setInstance(this);//####[63]####
        return TaskpoolFactory.getTaskpool().enqueueMulti(taskinfo, -1);//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public TaskIDGroup<Void> arrayJoiner(TaskID<Iterator<Double[][]>> it) {//####[63]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[63]####
        return arrayJoiner(it, new TaskInfo());//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public TaskIDGroup<Void> arrayJoiner(TaskID<Iterator<Double[][]>> it, TaskInfo taskinfo) {//####[63]####
        // ensure Method variable is set//####[63]####
        if (__pt__arrayJoiner_IteratorDoubleArAr_method == null) {//####[63]####
            __pt__arrayJoiner_IteratorDoubleArAr_ensureMethodVarSet();//####[63]####
        }//####[63]####
        taskinfo.setTaskIdArgIndexes(0);//####[63]####
        taskinfo.addDependsOn(it);//####[63]####
        taskinfo.setParameters(it);//####[63]####
        taskinfo.setMethod(__pt__arrayJoiner_IteratorDoubleArAr_method);//####[63]####
        taskinfo.setInstance(this);//####[63]####
        return TaskpoolFactory.getTaskpool().enqueueMulti(taskinfo, -1);//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public TaskIDGroup<Void> arrayJoiner(BlockingQueue<Iterator<Double[][]>> it) {//####[63]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[63]####
        return arrayJoiner(it, new TaskInfo());//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public TaskIDGroup<Void> arrayJoiner(BlockingQueue<Iterator<Double[][]>> it, TaskInfo taskinfo) {//####[63]####
        // ensure Method variable is set//####[63]####
        if (__pt__arrayJoiner_IteratorDoubleArAr_method == null) {//####[63]####
            __pt__arrayJoiner_IteratorDoubleArAr_ensureMethodVarSet();//####[63]####
        }//####[63]####
        taskinfo.setQueueArgIndexes(0);//####[63]####
        taskinfo.setIsPipeline(true);//####[63]####
        taskinfo.setParameters(it);//####[63]####
        taskinfo.setMethod(__pt__arrayJoiner_IteratorDoubleArAr_method);//####[63]####
        taskinfo.setInstance(this);//####[63]####
        return TaskpoolFactory.getTaskpool().enqueueMulti(taskinfo, -1);//####[63]####
    }//####[63]####
    /**
	 * This method takes a 2D Double array from the iterator to join
	 * This method is parallelised for SIMD
	 * 
	 * @param it	the ParaIterator of the data set to be joined
	 *///####[63]####
    public void __pt__arrayJoiner(Iterator<Double[][]> it) {//####[63]####
        while (it.hasNext()) //####[65]####
        {//####[65]####
            Double[][] arraysToJoin = it.next();//####[67]####
            Double[] output = arraysToJoin[0];//####[69]####
            for (int i = 1; i < arraysToJoin.length; i++) //####[71]####
            {//####[71]####
                Double[] current = arraysToJoin[i];//####[72]####
                Double[] temp = Arrays.copyOf(output, output.length + current.length);//####[75]####
                for (int j = output.length; j < temp.length; j++) //####[76]####
                {//####[76]####
                    temp[j] = current[j - output.length];//####[77]####
                }//####[78]####
                output = temp;//####[79]####
            }//####[80]####
        }//####[83]####
    }//####[85]####
//####[85]####
//####[93]####
    /**
	 * This method initialises a data set of 2D Double arrays to be joined by arrayJoiner
	 * 
	 * @param x	the number of 2D Double arrays to create
	 * @return	a ParaIterator over the arraylist of 2D Double arrays
	 *///####[93]####
    public Iterator<Double[][]> initialiseArrayJoinerDataSet(int x) {//####[93]####
        List<Double[][]> arraysToJoin = new ArrayList<Double[][]>();//####[94]####
        Double[][] iArr1 = null;//####[95]####
        for (int i = 0; i < x; i++) //####[96]####
        {//####[96]####
            int numArrays = 5;//####[98]####
            iArr1 = new Double[numArrays][];//####[99]####
            for (int j = 0; j < numArrays; j++) //####[100]####
            {//####[100]####
                int arraySize = 10;//####[102]####
                iArr1[j] = this.createRandomDataset(arraySize);//####[103]####
            }//####[104]####
            arraysToJoin.add(iArr1);//####[105]####
        }//####[107]####
        int numThreads = Runtime.getRuntime().availableProcessors();//####[109]####
        Iterator<Double[][]> it = ParIteratorFactory.createParIterator(arraysToJoin, numThreads);//####[111]####
        return it;//####[113]####
    }//####[114]####
//####[122]####
    /**
	 * Creates an array of doubles filled with random numbers
     * 
     * @param size	the size of the set to be returned
     * @return 	An array of random numbers
     *///####[122]####
    private Double[] createRandomDataset(int size) {//####[122]####
        List<Double> randomList = new LinkedList<Double>();//####[123]####
        for (int i = size - 1; i >= 0; i--) //####[124]####
        {//####[124]####
            randomList.add(ThreadLocalRandom.current().nextDouble());//####[125]####
        }//####[126]####
        return randomList.toArray(new Double[0]);//####[127]####
    }//####[128]####
//####[136]####
    /**
	 * This method initialises a data set of integer arrays to be sorted
	 * 
	 * @param x	The case for the type of data set to be created
	 * @return	a ParaIterator over the different arrays to be sorted
	 *///####[136]####
    public Iterator<int[]> initialiseMergeSortDataSet(int x) {//####[136]####
        ArrayUtils arrayUtil = new ArrayUtils();//####[137]####
        List<int[]> arraysToSort = new ArrayList<int[]>();//####[138]####
        switch(x) {//####[139]####
            case 0://####[139]####
                for (int i = 0; i < 10000; i++) //####[142]####
                {//####[142]####
                    int size = 5000;//####[143]####
                    arraysToSort.add(arrayUtil.orderedArray(size));//####[144]####
                }//####[145]####
                break;//####[146]####
            case 1://####[146]####
                for (int i = 0; i < 10000; i++) //####[149]####
                {//####[149]####
                    int size = 5000;//####[150]####
                    arraysToSort.add(arrayUtil.reversedArray(size));//####[151]####
                }//####[152]####
                break;//####[153]####
            case 2://####[153]####
                for (int i = 0; i < 10000; i++) //####[156]####
                {//####[156]####
                    int size = 5000;//####[157]####
                    arraysToSort.add(arrayUtil.singleValueArray(size));//####[158]####
                }//####[159]####
                break;//####[160]####
            case 3://####[160]####
                for (int i = 0; i < 10000; i++) //####[163]####
                {//####[163]####
                    int size = 5000;//####[164]####
                    arraysToSort.add(arrayUtil.shuffledNoDuplicatesArray(size));//####[165]####
                }//####[166]####
                break;//####[167]####
            case 4://####[167]####
                for (int i = 0; i < 10000; i++) //####[170]####
                {//####[170]####
                    int size = 5000;//####[171]####
                    arraysToSort.add(arrayUtil.randomArray(size));//####[172]####
                }//####[173]####
                break;//####[174]####
            case 5://####[174]####
                for (int i = 0; i < 1000000; i++) //####[177]####
                {//####[177]####
                    int size = 100;//####[178]####
                    arraysToSort.add(arrayUtil.randomArray(size));//####[179]####
                }//####[180]####
                break;//####[181]####
            case 6://####[181]####
                for (int i = 0; i < 12; i++) //####[184]####
                {//####[184]####
                    int size = 20000;//####[185]####
                    arraysToSort.add(arrayUtil.randomArray(size));//####[186]####
                }//####[187]####
                break;//####[188]####
        }//####[188]####
        int numThreads = Runtime.getRuntime().availableProcessors();//####[191]####
        Iterator<int[]> it = ParIteratorFactory.createParIterator(arraysToSort, numThreads);//####[193]####
        return it;//####[195]####
    }//####[196]####
//####[203]####
    private static volatile Method __pt__doMerge_IteratorintAr_method = null;//####[203]####
    private synchronized static void __pt__doMerge_IteratorintAr_ensureMethodVarSet() {//####[203]####
        if (__pt__doMerge_IteratorintAr_method == null) {//####[203]####
            try {//####[203]####
                __pt__doMerge_IteratorintAr_method = ParaTaskHelper.getDeclaredMethod(new ParaTaskHelper.ClassGetter().getCurrentClass(), "__pt__doMerge", new Class[] {//####[203]####
                    Iterator.class//####[203]####
                });//####[203]####
            } catch (Exception e) {//####[203]####
                e.printStackTrace();//####[203]####
            }//####[203]####
        }//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public TaskIDGroup<Void> doMerge(Iterator<int[]> it) {//####[203]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[203]####
        return doMerge(it, new TaskInfo());//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public TaskIDGroup<Void> doMerge(Iterator<int[]> it, TaskInfo taskinfo) {//####[203]####
        // ensure Method variable is set//####[203]####
        if (__pt__doMerge_IteratorintAr_method == null) {//####[203]####
            __pt__doMerge_IteratorintAr_ensureMethodVarSet();//####[203]####
        }//####[203]####
        taskinfo.setParameters(it);//####[203]####
        taskinfo.setMethod(__pt__doMerge_IteratorintAr_method);//####[203]####
        taskinfo.setInstance(this);//####[203]####
        return TaskpoolFactory.getTaskpool().enqueueMulti(taskinfo, -1);//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public TaskIDGroup<Void> doMerge(TaskID<Iterator<int[]>> it) {//####[203]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[203]####
        return doMerge(it, new TaskInfo());//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public TaskIDGroup<Void> doMerge(TaskID<Iterator<int[]>> it, TaskInfo taskinfo) {//####[203]####
        // ensure Method variable is set//####[203]####
        if (__pt__doMerge_IteratorintAr_method == null) {//####[203]####
            __pt__doMerge_IteratorintAr_ensureMethodVarSet();//####[203]####
        }//####[203]####
        taskinfo.setTaskIdArgIndexes(0);//####[203]####
        taskinfo.addDependsOn(it);//####[203]####
        taskinfo.setParameters(it);//####[203]####
        taskinfo.setMethod(__pt__doMerge_IteratorintAr_method);//####[203]####
        taskinfo.setInstance(this);//####[203]####
        return TaskpoolFactory.getTaskpool().enqueueMulti(taskinfo, -1);//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public TaskIDGroup<Void> doMerge(BlockingQueue<Iterator<int[]>> it) {//####[203]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[203]####
        return doMerge(it, new TaskInfo());//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public TaskIDGroup<Void> doMerge(BlockingQueue<Iterator<int[]>> it, TaskInfo taskinfo) {//####[203]####
        // ensure Method variable is set//####[203]####
        if (__pt__doMerge_IteratorintAr_method == null) {//####[203]####
            __pt__doMerge_IteratorintAr_ensureMethodVarSet();//####[203]####
        }//####[203]####
        taskinfo.setQueueArgIndexes(0);//####[203]####
        taskinfo.setIsPipeline(true);//####[203]####
        taskinfo.setParameters(it);//####[203]####
        taskinfo.setMethod(__pt__doMerge_IteratorintAr_method);//####[203]####
        taskinfo.setInstance(this);//####[203]####
        return TaskpoolFactory.getTaskpool().enqueueMulti(taskinfo, -1);//####[203]####
    }//####[203]####
    /**
	 * Mergesort of different arrays to be done in parallel
	 * 
	 * @param it	the ParaIterator over the data set of arrays to be sorted
	 *///####[203]####
    public void __pt__doMerge(Iterator<int[]> it) {//####[203]####
        while (it.hasNext()) //####[204]####
        {//####[204]####
            int[] arrayToSort = it.next();//####[205]####
            SIMDMergeSort merger = new SIMDMergeSort(arrayToSort.length);//####[206]####
            merger.partition(arrayToSort, 0, arrayToSort.length - 1);//####[208]####
        }//####[210]####
    }//####[211]####
//####[211]####
}//####[211]####
