package com.uoa.se751.paraTaskProject.trace;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import com.uoa.se751.paraTaskProject.trace.util.MethodContext;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

/**
 * This class will transform each class that we want to instrumentalise. By
 * transforming each class, we can inject code that will give information about
 * the processes and threads that run each method call.
 * 
 * @author John Ramirez, Akram Darwazeh, Hailun Wang and Henry Wu
 *
 */
public abstract class ClassTransformer implements ClassFileTransformer {

	private String className;
	private MethodContext[] methodNames;
	private final String _mTaskGroupID = "mTaskGroupID";
	private final String _taskID = "taskID";
	private final String _relativeTaskID = "relativeTaskID";
	private final String _taskName = "taskName";
	private final String _threadID = "threadID";
	private final String _startTime = "startTime";
	private final String _endTime = "endTime";
	private final String _infiltrator = "infiltrator";

	public ClassTransformer() {
		this.className = defineClassName();
		this.methodNames = methodsToInstrument();
	}

	/**
	 * Overridden method from interface
	 * 
	 * @see ClassFileTransformer
	 */
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classFileBuffer) throws IllegalClassFormatException {
		byte[] byteCode = classFileBuffer;

		if (className.equals(this.className)) {
			try {
				ClassPool cp = ClassPool.getDefault();
				CtClass cc = cp.get(modifyClassName(this.className));
				CtClass infiltrator = cp.get(Infiltrator.class.getCanonicalName());
				CtClass stringClass = cp.get(String.class.getCanonicalName());

				for (MethodContext method : this.methodNames) {
					CtMethod m = cc.getDeclaredMethod(method.getMethodName());
					m.addLocalVariable(_threadID, CtClass.longType);
					// If this task is not a paratask parallel task then we do
					// not want to track task information.
					this.instantiateInfiltrator(m, infiltrator);
					if (method.isParallelTask())
						this.setUpTaskManager(m, stringClass);

					m.insertBefore(_threadID + " = Thread.currentThread().getId();");
					this.setUpTiming(m);
					this.infiltrate(m, method);
				}

				byteCode = cc.toBytecode();
				cc.detach();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return byteCode;
	}

	/**
	 * Inserts code into tasks that will track ParaTask's tasking behaviors
	 * 
	 * @param m
	 *            The method we will be injecting tracing code into
	 * @param string
	 *            the java string class
	 * @throws CannotCompileException
	 */
	private void setUpTaskManager(CtMethod m, CtClass string) throws CannotCompileException {
		// Add local variables
		m.addLocalVariable(_mTaskGroupID, CtClass.intType);
		m.addLocalVariable(_taskID, CtClass.intType);
		// Relative task ID is the ID of the task from within the group
		// E.g task group 0, task ID 1, relative task ID 0
		// This means that task 1 is subtask 0 of group 0
		m.addLocalVariable(_relativeTaskID, CtClass.intType);
		m.addLocalVariable(_taskName, string);

		// Task tracking code that will be inserted by our agent
		StringBuilder sb = new StringBuilder();
//		sb.append('{');
//		sb.append("if(pt.runtime.CurrentTask.isMultiTask()){");
//		sb.append(_mTaskGroupID + " = pt.runtime.CurrentTask.currentTaskID().getGroup().globalID();");
//		sb.append("} else {");
//		sb.append(_mTaskGroupID + " = -1;");
//		sb.append('}');
		sb.append(_taskID + " = pt.runtime.CurrentTask.globalID();");
//		sb.append(_relativeTaskID + " = pt.runtime.CurrentTask.relativeID();");
		sb.append(_taskName + " = pt.runtime.CurrentTask.currentTaskID().getTaskName();");
//		sb.append('}');

		m.insertBefore(sb.toString());
	}

	/**
	 * Inserts code into tasks that will track when a method started and ended
	 * 
	 * @param m
	 *            the method we will be injecting tracing code into
	 * @throws CannotCompileException
	 */
	private void setUpTiming(CtMethod m) throws CannotCompileException {
		m.addLocalVariable(_startTime, CtClass.longType);
		m.addLocalVariable(_endTime, CtClass.longType);

		StringBuilder sb = new StringBuilder();
		sb.append(_startTime + " = System.nanoTime()/1000000;");
		m.insertBefore(sb.toString());
		sb = new StringBuilder();
		sb.append(_endTime + " = System.nanoTime()/1000000;");
		m.insertAfter(sb.toString());
	}

	/**
	 * Injects and instantiates the infiltrator into the method that we want to trace
	 * 
	 * @see Infiltrator
	 * @param m
	 *            the method we will be injecting the infiltrator into
	 * @param infiltrator
	 *            the Infiltrator object that will be injected and instantiated in the method
	 * @throws CannotCompileException
	 */
	private void instantiateInfiltrator(CtMethod m, CtClass infiltrator) throws CannotCompileException {
		m.addLocalVariable(_infiltrator, infiltrator);
		StringBuilder sb = new StringBuilder();
		sb.append(_infiltrator + " = " + Infiltrator.class.getCanonicalName() + ".getInstance();");
		m.insertAfter(sb.toString());
	}
	
	/**
	 * Inifiltrates the method and passes variables which have been assigned to be transcribed into Paje
	 * 
	 * @param m
	 * @throws CannotCompileException
	 */
	private void infiltrate(CtMethod m, MethodContext method) throws CannotCompileException {
		if (method.isParallelTask()) {
			m.insertAfter(_infiltrator + ".addThreadAndTaskInfo(" + _threadID + ", " + _taskID + ", " + _taskName + ", " + _startTime + ");");
			m.insertAfter(_infiltrator + ".destroyThreadAndTaskInfo(" + _threadID + ", " + _taskID + ", " + _taskName + ", " + _endTime + ");");
		} else {
			m.insertAfter(_infiltrator + ".addThreadInfo(" + _threadID + ", " + _startTime + ");");
			m.insertAfter(_infiltrator + ".destroyThreadInfo(" + _threadID + ", " + _endTime + ");");
		}
	}

	/**
	 * This method returns the full class path with "/" replaced with "."
	 * 
	 * @param className
	 *            the class name you want to modify
	 * @return the modified class path
	 */
	protected String modifyClassName(String className) {
		return className.replaceAll("/", ".");
	}

	/**
	 * Defines the full class path of a class with the root package being the
	 * first element E.g. com/uoa/se751/paraTaskProject/SIMD
	 * 
	 * @return the full class path
	 */
	protected abstract String defineClassName();

	/**
	 * The method name of every method that you want to instrumentalise should
	 * be put into a string array in this method
	 * 
	 * @return string array of methods you want to instrumentalise
	 */
	protected abstract MethodContext[] methodsToInstrument();
}
