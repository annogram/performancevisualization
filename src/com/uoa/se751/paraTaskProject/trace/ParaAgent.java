package com.uoa.se751.paraTaskProject.trace;

import java.lang.instrument.Instrumentation;

/**
 * If the string "-javaagent:agent.jar" is included in the launch parameters,
 * this class will be instantiated so that instrumentation can be done.
 * @author Akram Darwazeh & Henry Wu
 *
 */
public class ParaAgent {
	public static volatile Instrumentation globalInstr;
	public static void premain(String args, Instrumentation inst){
		globalInstr = inst;
	}
}
