package com.uoa.se751.paraTaskProject.trace;

import com.uoa.se751.paraTaskProject.trace.util.StringTuple;

/**
 * PajeTranscriber formats the output of the results to
 * a string that is in PAJE format.
 * @author Akram Darwazeh, John Ramirez, Hailun Wang and Henry Wu
 *
 */
public class PajeTranscriber {
	
	private final String hStart = "%EventDef", hEnd = "%EndEventDef\n";
	private long programStartTime = 0;
	private int fixedCharStartIndex;
	private int fixedCharEndIndex;
	
	/**{
	 * 
	 * This methods generates the Paje headers
	 */
	public void generateHeaders(StringBuilder _sb) {
		_sb.append(this.createPHeader(PajeHeader.PajeDefineContainerType, 
				new StringTuple("Alias", "string"),
				new StringTuple("ContainerType", "string"), 
				new StringTuple("Name", "string")));
		_sb.append(this.createPHeader(PajeHeader.PajeDefineStateType,
				new StringTuple("Alias", "string"),
				new StringTuple("ContainerType", "string"),
				new StringTuple("Name", "string")));
		_sb.append(this.createPHeader(PajeHeader.PajeDefineEntityValue,
				new StringTuple("Alias", "string"),
				new StringTuple("EntityType", "string"),
				new StringTuple("Name", "string")));
		_sb.append(this.createPHeader(PajeHeader.PajeCreateContainer,
				new StringTuple("Time", "date"),
				new StringTuple("Alias", "string"),
				new StringTuple("Type", "string"),
				new StringTuple("Container", "string"),
				new StringTuple("Name", "string")));
		_sb.append(this.createPHeader(PajeHeader.PajeDestroyContainer,
				new StringTuple("Time", "date"),
				new StringTuple("Name", "string"),
				new StringTuple("Type", "string")));
		_sb.append(this.createPHeader(PajeHeader.PajeSetState,
				new StringTuple("Time", "date"),
				new StringTuple("Type", "string"),
				new StringTuple("Container", "string"),
				new StringTuple("Value", "string")));
	}
	
	/**
	 * This method initialises relevant definitions and initial start time for the Paje trace file
	 * @param _sb
	 */
	public void initialiseDefinitions(StringBuilder _sb) {
		_sb.append("1 P 0 Program\n");
		_sb.append("1 T P Threads\n");
		_sb.append("1 TK T Tasks\n");
		_sb.append("3 ThS T \"Thread State\"\n");
		_sb.append("3 TaS TK \"Task State\"\n");
		_sb.append("6 E1 ThS Executing\n");
		_sb.append("6 E2 TaS Executing\n");
		this.fixedCharStartIndex = _sb.length();
		_sb.append("7 00000000 PC P 0 \"Program\"\n");
		this.fixedCharEndIndex = _sb.length();
	}
	
	/**
	 * Replaces the initial program start time with earliest thread start time
	 * @param _sb
	 */
	public void replacePajeProgramStartTime(StringBuilder _sb) {
		
		_sb.replace(fixedCharStartIndex, fixedCharEndIndex, "7 " + programStartTime + " PC P 0 \"Program\"\n");
	}
	
	/**
	 * Sets program start time variable
	 * @param startTime
	 */
	public void setProgramStartTime(long startTime) {
		this.programStartTime = startTime;
	}
	
	/**
	 * This method creates Paje containers for ParaTask threads and tasks, as well as setting the states for them
	 * 
	 * @param _sb
	 * @param threadId
	 * @param taskId
	 * @param taskName
	 * @param startTime
	 */
	public void createThreadAndTaskContainers(StringBuilder _sb, long threadId, int taskId, String taskName, long startTime) {
		_sb.append("7 " + startTime + " T" + threadId + " T PC \"Thread " + threadId + "\"\n");
		_sb.append("10 " + startTime + " ThS T" + threadId + " E1\n");
		_sb.append("7 " + startTime + " T" + threadId + "TK" + taskId + " TK T" + threadId + " \"Task ID " + taskId + " " + taskName + "\"\n");
		_sb.append("10 " + startTime + " TaS T" + threadId + "TK" + taskId + " E2\n");
	}
	
	/**
	 * This method destroys the Paje containers for ParaTask threads and tasks
	 * 
	 * @param _sb
	 * @param threadId
	 * @param taskId
	 * @param taskName
	 * @param endTime
	 */
	public void destroyThreadAndTaskContainers(StringBuilder _sb, long threadId, int taskId, String taskName, long endTime) {
		_sb.append("8 " + endTime + " T" + threadId + "TK" + taskId + " TK\n");
		_sb.append("8 " + endTime + " T" + threadId +  " T\n");
	}
	
	/**
	 * This method creates Paje containers for non-ParaTask threads, as well as setting the states for them
	 * 
	 * @param _sb
	 * @param threadId
	 * @param startTime
	 */
	public void createThreadContainer(StringBuilder _sb, long threadId, long startTime) {
		_sb.append("7 " + startTime + " T" + threadId + " T PC \"Thread " + threadId + "\"\n");
		_sb.append("10 " + startTime + " ThS T" + threadId + " E1\n");
	}
	
	/**
	 * This method destroys the Paje containers for non-ParaTask threads
	 * 
	 * @param _sb
	 * @param threadId
	 * @param endTime
	 */
	public void destroyThreadContainer(StringBuilder _sb, long threadId, long endTime) {
		_sb.append("8 " + endTime + " T" + threadId +  " T\n");
	}
	
	/**
	 * Creates Paje headers
	 * 
	 * @param p
	 * @param descriptions
	 * @return
	 */
	private String createPHeader(PajeHeader p, StringTuple... descriptions) {
		StringBuilder sb = new StringBuilder();
		sb.append(hStart + "\t" + p.toString() + " " + p.getValue() + "\n");
		for (StringTuple s : descriptions) {
			sb.append("%\t" + s.x + " " + s.y + "\n");
		}
		sb.append(hEnd);
		return sb.toString();
	}
}
