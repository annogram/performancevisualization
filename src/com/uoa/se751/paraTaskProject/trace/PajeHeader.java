package com.uoa.se751.paraTaskProject.trace;

/**
 * Paje has meta value headers when creating a trace file, these
 * have been abstracted as enums
 * @author Akram Darwazeh & John Ramirez
 *
 */
public enum PajeHeader {
	PajeDefineContainerType(1), PajeDefineStateType(3), PajeDefineEntityValue(6), PajeCreateContainer(
			7), PajeDestroyContainer(8), PajeSetState(10);

	private int value = 0;

	PajeHeader(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

}
