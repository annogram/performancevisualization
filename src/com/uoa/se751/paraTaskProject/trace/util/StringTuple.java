package com.uoa.se751.paraTaskProject.trace.util;

public class StringTuple {
	public final String x;
	public final String y;

	public StringTuple(String x, String y) {
		this.x = x;
		this.y = y;
	}
}
