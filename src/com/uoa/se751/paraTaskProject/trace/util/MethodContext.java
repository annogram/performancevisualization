package com.uoa.se751.paraTaskProject.trace.util;

/**
 * MethodContext is a structural class which will store the values of methods
 * that want to be traced
 * 
 * @author Akram Darwazeh
 *
 */
public class MethodContext {
	private String _methodName;
	private boolean _isParallelTask;

	/**
	 * Default constructor initialized to a null value which can be changed
	 * through the getters and setters.
	 */
	public MethodContext() {
		_isParallelTask = false;
		_methodName = null;
	}

	/**
	 * Construct a MethodContext object with and identify weather or not the
	 * method being inspected is a parallel task or a sequential one.
	 * 
	 * @param methodName The name of the method being traced
	 * @param isPara True if the task is a parallel task else the task is sequential
	 */
	public MethodContext(String methodName, boolean isPara) {
		_isParallelTask = isPara;
		_methodName = methodName;
	}

	public String getMethodName() {
		if (_isParallelTask)
			return "__pt__" + _methodName;
		return _methodName;
	}

	public boolean isParallelTask() {
		return _isParallelTask;
	}

	public void setMethodName(String methodName) {
		this._methodName = methodName;
	}

	public void setParallelTask(boolean isParallelTask) {
		this._isParallelTask = isParallelTask;
	}
}
