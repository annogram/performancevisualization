package com.uoa.se751.paraTaskProject.trace;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Objects called from this Singleton will be injected into an object to get the
 * instrumented data from the monitored classes.
 * 
 * Data from the instrumented class will be written to a .trace file that the
 * visulization tool Paje can read in.
 * 
 * This infiltrator should never be called inside of a project and should only
 * be injected through the ClassTransformer.
 * 
 * @author Akram Darwazeh, John Ramirez, Hailun Wang and Henry Wu
 * @see com.uoa.se751.paraTaskProject.trace.ClassTransformer
 */
public class Infiltrator {
	public static volatile Infiltrator i = null;
	private StringBuilder _sb = null;
	private PajeTranscriber pj = null;
	private AtomicLong _programStartTime = new AtomicLong();

	private Infiltrator() {
		_sb = new StringBuilder();
		pj = new PajeTranscriber();
		pj.generateHeaders(_sb);
		pj.initialiseDefinitions(_sb);
		
		// Writes the Paje trace file upon program execution completion
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				Infiltrator infiltrator = Infiltrator.getInstance();
				infiltrator.writeToFile();
			}
		});
	}
	
	/**
	 * @return singleton of Infiltrator
	 */
	public static synchronized Infiltrator getInstance() {
		if (i == null)
			i = new Infiltrator();
		return i;
	}
	
	/**
	 * Writes the Paje trace to file to out.trace in the "res" directory
	 */
	public void writeToFile() {
		// Replaces program start time with earliest thread start time
		pj.setProgramStartTime(_programStartTime.get());
		pj.replacePajeProgramStartTime(_sb);
		
		String filePath = "res";
		File f = new File(filePath);
		BufferedWriter bf = null;
		f.mkdirs();
		try {
			bf = new BufferedWriter(new FileWriter(f.getAbsolutePath() + "/out.trace"));
			bf.write(_sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * This method passes ParaTask thread and task start information to the Paje transcriber
	 * 
	 * @param threadId
	 * @param taskId
	 * @param taskName
	 * @param startTime
	 */
	public synchronized void addThreadAndTaskInfo(long threadId, int taskId, String taskName, long startTime) {
		//call createContianers
		this.checkProgramStartTime(startTime);
		pj.createThreadAndTaskContainers(_sb, threadId, taskId, taskName, startTime);
	}
	
	/**
	 * This method passes ParaTask thread and task end information to the Paje transcriber
	 * 
	 * @param threadId
	 * @param taskId
	 * @param taskName
	 * @param endTime
	 */
	public synchronized void destroyThreadAndTaskInfo(long threadId, int taskId, String taskName, long endTime) {
		pj.destroyThreadAndTaskContainers(_sb, threadId, taskId, taskName, endTime);
	}
	
	/**
	 * This method passes non-ParaTask thread start information to the Paje transcriber
	 * 
	 * @param threadId
	 * @param startTime
	 */
	public synchronized void addThreadInfo(long threadId, long startTime) {
		this.checkProgramStartTime(startTime);
		pj.createThreadContainer(_sb, threadId, startTime);
	}
	
	/**
	 * This method passes non-ParaTask thread end information to the Paje transcriber
	 * 
	 * @param threadId
	 * @param endTime
	 */
	public synchronized void destroyThreadInfo(long threadId, long endTime) {
		pj.destroyThreadContainer(_sb, threadId, endTime);
	}
	
	/**
	 * This method updates the AtomicLong to the earliest start time
	 * 
	 * @param startTime
	 */
	private void checkProgramStartTime(long startTime) {
		if (_programStartTime.get() == 0) {
			_programStartTime.set(startTime);
		} else if (startTime < _programStartTime.get()) {
			_programStartTime.set(startTime);
		}
	}
}