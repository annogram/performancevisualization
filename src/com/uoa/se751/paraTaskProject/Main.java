package com.uoa.se751.paraTaskProject;

import java.util.Iterator;

import com.uoa.se751.paraTaskProject.trace.ParaAgent;

import pt.runtime.TaskID;

public class Main {

	public static void main(String[] args) {
		ParaAgent.globalInstr.addTransformer(new SIMDTransformer());
		SIMD simd = new SIMD();
		
		// This section performs the arrayJoiner SIMD operations
		Iterator<Double[][]> it = simd.initialiseArrayJoinerDataSet(20);
		
		long startTime = System.currentTimeMillis();
		
		TaskID<Void> joinerTasksId = simd.arrayJoiner(it);
		
		try{
			joinerTasksId.waitTillFinished();
		} catch (Exception e){
			e.printStackTrace();
		}
		
		long endTime = System.currentTimeMillis();
		
		System.out.println("Joining took: " + (endTime - startTime) + " milliseconds");

		
		// Uncomment the code below for MergeSort SIMD operations
		
		// Initialise merge sort data set
//		Iterator<int[]> it = simd.initialiseMergeSortDataSet(6);
//
//		long startTime = System.currentTimeMillis();
//		
//		TaskID<Void> mergeTasksId = simd.doMerge(it);
//		
//		try{
//			mergeTasksId.waitTillFinished();
//		} catch (Exception e){
//			e.printStackTrace();
//		}
//		
//		long endTime = System.currentTimeMillis();
//
//		System.out.println("Sorting with TASK took: " + (endTime - startTime) + " milliseconds");
	}
}
