package com.uoa.se751.paraTaskProject;

/**
 * Implementation of a sequential merge sort to be used in SIMD parallelisation
 * 
 * @author John Ramirez
 *
 */
public class SIMDMergeSort {

	private int[] temp;
	
	//Default constructor
	public SIMDMergeSort(int length) {
		this.temp = new int[length];
	}
	
	/**
	 * This method recursively splits the input array in half
	 * 
	 * @param array			array to be split in half
	 * @param startIndex	the start index of the array
	 * @param endIndex		the end index of the array
	 */
	public void partition(int[] array, int startIndex, int endIndex) {
		if (startIndex < endIndex) {
			int middleIndex = startIndex + (endIndex - startIndex) / 2;
			
			//sorts the left side of the array
			partition(array, startIndex, middleIndex);
			
			//sorts the right side of the array
			partition(array, middleIndex + 1, endIndex);

			//merge both sides
			merge(array, startIndex, middleIndex, endIndex);
		}
	}
	
	/**
	 * This method compares, sorts and merges the sub-arrays into a single array
	 * 
	 * @param array
	 * @param start
	 * @param middle	
	 * @param end	
	 */
	public void merge(int[] array, int start, int middle, int end) {
		int[] temp = new int[array.length];
		
		for (int i = start; i <= end; i++) {
			temp[i] = array[i];
		}
		
		int i = start;
		int j = middle + 1;
		int k = start;
		
		//Copy smallest values from left or right side to the array
		while (i <= middle && j <= end) {
			if (temp[i] <= temp[j]) {
				array[k] = temp[i];
				i++;
			} else {
				array[k] = temp[j];
				j++;
			}
			k++;
		}
		
		//Copies rest of the left side to array
		while (i <= middle) {
			array[k] = temp[i];
			k++;
			i++;
		}
	}
}
