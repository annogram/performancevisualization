package com.uoa.se751.paraTaskProject;

import com.uoa.se751.paraTaskProject.trace.ClassTransformer;
import com.uoa.se751.paraTaskProject.trace.util.MethodContext;

public class SIMDTransformer extends ClassTransformer {

	@Override
	protected String defineClassName() {
		return "com/uoa/se751/paraTaskProject/SIMD";
	}

	@Override
	protected MethodContext[] methodsToInstrument() {
		//return new MethodContext[]{new MethodContext("initialiseMergeSortDataSet", false), new MethodContext("doMerge", true)};
		return new MethodContext[] {new MethodContext("initialiseArrayJoinerDataSet", false), new MethodContext("arrayJoiner", true)};
	}
}
