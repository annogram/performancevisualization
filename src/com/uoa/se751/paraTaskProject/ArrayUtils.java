package com.uoa.se751.paraTaskProject;

import java.util.ArrayList;
import java.util.Random;

/**
 * Creates arrays with different properties to be used when testing.
 * 
 * @author Hailun Wang
 */

public class ArrayUtils {

	//ordered array
	public int[] orderedArray(int size){
		int[] orderedArray = new int[size];
		for(int i = 0; i < size; i++) {
			orderedArray[i] = i;
		}
		return orderedArray;
	}
	
	//array in reverse order
	public int[] reversedArray(int size){
		int[] reversedArray = new int[size];
		for(int i = 0; i < size; i++){
			reversedArray[i] = size - 1 - i;
		}
		return reversedArray;
	}
	
	//array of only one number
	public int[] singleValueArray(int size){
		int[] singleValueArray = new int[size];
		Random rand = new Random();
		int value = rand.nextInt(size);
		for(int i = 0; i < size; i++){
			singleValueArray[i] = value;
		}
		
		return singleValueArray;
	}
	
	//Random/shuffled array containing no duplicates
	public int[] shuffledNoDuplicatesArray(int size){
		ArrayList<Integer> arrList = new ArrayList<Integer>();
		for(int i = 0; i < size; i++){
			arrList.add(i);
		}
		
		Random rand = new Random();
		int[] shuffledNoDupArr = new int[size];
		for(int i = 0; i < size; i++){
			shuffledNoDupArr[i] = arrList.remove(rand.nextInt(size - i));
		}
		
		return shuffledNoDupArr;
	}
	
	//random array
	public int[] randomArray(int size){
		int[] randomArray = new int[size];
		Random rand = new Random();
		for(int i = 0; i < size; i++){
			randomArray[i] = rand.nextInt(size);
		}
		
		return randomArray;
	}
}
