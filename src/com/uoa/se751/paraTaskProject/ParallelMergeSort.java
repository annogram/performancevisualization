package com.uoa.se751.paraTaskProject;//####[1]####
//####[1]####
import pt.runtime.TaskID;//####[3]####
import pt.runtime.TaskIDGroup;//####[4]####
//####[4]####
//-- ParaTask related imports//####[4]####
import pt.runtime.*;//####[4]####
import java.util.concurrent.ExecutionException;//####[4]####
import java.util.concurrent.locks.*;//####[4]####
import java.lang.reflect.*;//####[4]####
import pt.runtime.GuiThread;//####[4]####
import java.util.concurrent.BlockingQueue;//####[4]####
import java.util.ArrayList;//####[4]####
import java.util.List;//####[4]####
//####[4]####
public class ParallelMergeSort {//####[6]####
    static{ParaTask.init();}//####[6]####
    /*  ParaTask helper method to access private/protected slots *///####[6]####
    public void __pt__accessPrivateSlot(Method m, Object instance, TaskID arg, Object interResult ) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {//####[6]####
        if (m.getParameterTypes().length == 0)//####[6]####
            m.invoke(instance);//####[6]####
        else if ((m.getParameterTypes().length == 1))//####[6]####
            m.invoke(instance, arg);//####[6]####
        else //####[6]####
            m.invoke(instance, arg, interResult);//####[6]####
    }//####[6]####
//####[8]####
    public void sequentialMergeSort(int[] array, int startIndex, int endIndex) {//####[8]####
        if (startIndex < endIndex) //####[9]####
        {//####[9]####
            int middleIndex = startIndex + (endIndex - startIndex) / 2;//####[10]####
            sequentialMergeSort(array, startIndex, middleIndex);//####[13]####
            sequentialMergeSort(array, middleIndex + 1, endIndex);//####[16]####
            doSequentialMerge(array, startIndex, middleIndex, endIndex);//####[19]####
        }//####[20]####
    }//####[21]####
//####[23]####
    private static volatile Method __pt__parallelMergeSort_intAr_int_int_method = null;//####[23]####
    private synchronized static void __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet() {//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            try {//####[23]####
                __pt__parallelMergeSort_intAr_int_int_method = ParaTaskHelper.getDeclaredMethod(new ParaTaskHelper.ClassGetter().getCurrentClass(), "__pt__parallelMergeSort", new Class[] {//####[23]####
                    int[].class, int.class, int.class//####[23]####
                });//####[23]####
            } catch (Exception e) {//####[23]####
                e.printStackTrace();//####[23]####
            }//####[23]####
        }//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, int leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, int leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, int leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, int leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(0);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, int leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, int leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, TaskID<Integer> leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, TaskID<Integer> leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(1);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, TaskID<Integer> leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, TaskID<Integer> leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(0, 1);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, TaskID<Integer> leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, TaskID<Integer> leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(1);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, BlockingQueue<Integer> leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, BlockingQueue<Integer> leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(1);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, BlockingQueue<Integer> leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, BlockingQueue<Integer> leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(1);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(0);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, BlockingQueue<Integer> leftIndex, int rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, BlockingQueue<Integer> leftIndex, int rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0, 1);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, int leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, int leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(2);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, int leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, int leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(0, 2);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, int leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, int leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(2);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, TaskID<Integer> leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, TaskID<Integer> leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(1, 2);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, TaskID<Integer> leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, TaskID<Integer> leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setTaskIdArgIndexes(0, 1, 2);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, TaskID<Integer> leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, TaskID<Integer> leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(1, 2);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, BlockingQueue<Integer> leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, BlockingQueue<Integer> leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(1);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(2);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, BlockingQueue<Integer> leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, BlockingQueue<Integer> leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(1);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(0, 2);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, BlockingQueue<Integer> leftIndex, TaskID<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, BlockingQueue<Integer> leftIndex, TaskID<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0, 1);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(2);//####[23]####
        taskinfo.addDependsOn(rightIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, int leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, int leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, int leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, int leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(0);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, int leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, int leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0, 2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, TaskID<Integer> leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, TaskID<Integer> leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(1);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, TaskID<Integer> leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, TaskID<Integer> leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(0, 1);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, TaskID<Integer> leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, TaskID<Integer> leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0, 2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(1);//####[23]####
        taskinfo.addDependsOn(leftIndex);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, BlockingQueue<Integer> leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(int[] array, BlockingQueue<Integer> leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(1, 2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, BlockingQueue<Integer> leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(TaskID<int[]> array, BlockingQueue<Integer> leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(1, 2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setTaskIdArgIndexes(0);//####[23]####
        taskinfo.addDependsOn(array);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, BlockingQueue<Integer> leftIndex, BlockingQueue<Integer> rightIndex) {//####[23]####
        //-- execute asynchronously by enqueuing onto the taskpool//####[23]####
        return parallelMergeSort(array, leftIndex, rightIndex, new TaskInfo());//####[23]####
    }//####[23]####
    public TaskID<int[]> parallelMergeSort(BlockingQueue<int[]> array, BlockingQueue<Integer> leftIndex, BlockingQueue<Integer> rightIndex, TaskInfo taskinfo) {//####[23]####
        // ensure Method variable is set//####[23]####
        if (__pt__parallelMergeSort_intAr_int_int_method == null) {//####[23]####
            __pt__parallelMergeSort_intAr_int_int_ensureMethodVarSet();//####[23]####
        }//####[23]####
        taskinfo.setQueueArgIndexes(0, 1, 2);//####[23]####
        taskinfo.setIsPipeline(true);//####[23]####
        taskinfo.setParameters(array, leftIndex, rightIndex);//####[23]####
        taskinfo.setMethod(__pt__parallelMergeSort_intAr_int_int_method);//####[23]####
        taskinfo.setInstance(this);//####[23]####
        return TaskpoolFactory.getTaskpool().enqueue(taskinfo);//####[23]####
    }//####[23]####
    public int[] __pt__parallelMergeSort(int[] array, int leftIndex, int rightIndex) {//####[23]####
        int mid;//####[24]####
        if ((rightIndex - leftIndex) <= 4) //####[26]####
        {//####[26]####
            sequentialMergeSort(array, leftIndex, rightIndex);//####[27]####
            return array;//####[28]####
        } else if (leftIndex < rightIndex) //####[29]####
        {//####[29]####
            mid = (rightIndex + leftIndex) / 2;//####[30]####
            TaskID id1 = parallelMergeSort(array, 0, mid);//####[32]####
            TaskID id2 = parallelMergeSort(array, mid + 1, rightIndex);//####[33]####
            TaskIDGroup group = new TaskIDGroup(2);//####[35]####
            group.add(id1);//####[36]####
            group.add(id2);//####[37]####
            try {//####[39]####
                group.waitTillFinished();//####[40]####
            } catch (Exception e) {//####[41]####
                e.printStackTrace();//####[42]####
            }//####[43]####
            try {//####[45]####
                return merge((int[]) id1.getReturnResult(), (int[]) id2.getReturnResult());//####[46]####
            } catch (Exception e) {//####[47]####
                e.printStackTrace();//####[48]####
            }//####[49]####
        }//####[50]####
        return new int[] { array[leftIndex] };//####[52]####
    }//####[53]####
//####[53]####
//####[55]####
    public int[] merge(int[] firstArray, int[] secondArray) {//####[55]####
        int length1 = firstArray.length;//####[56]####
        int length2 = secondArray.length;//####[57]####
        int totalLength = length1 + length2;//####[58]####
        int startA = 0;//####[60]####
        int startB = 0;//####[61]####
        int index = 0;//####[62]####
        int[] result = new int[totalLength];//####[63]####
        while (startA < length1 && startB < length2) //####[66]####
        {//####[66]####
            if (firstArray[startA] < secondArray[startB]) //####[67]####
            {//####[67]####
                result[index] = firstArray[startA];//####[68]####
                startA++;//####[69]####
            } else {//####[70]####
                result[index] = secondArray[startB];//####[71]####
                startB++;//####[72]####
            }//####[73]####
            index++;//####[74]####
        }//####[75]####
        if (startA < length1) //####[78]####
        {//####[78]####
            for (int i = startA; i < length1; i++) //####[79]####
            {//####[79]####
                result[index] = firstArray[i];//####[80]####
                index++;//####[81]####
            }//####[82]####
        }//####[83]####
        if (startB < length2) //####[86]####
        {//####[86]####
            for (int i = startB; i < length2; i++) //####[87]####
            {//####[87]####
                result[index] = secondArray[i];//####[88]####
                index++;//####[89]####
            }//####[90]####
        }//####[91]####
        firstArray = null;//####[93]####
        secondArray = null;//####[94]####
        return result;//####[95]####
    }//####[96]####
//####[98]####
    public void doSequentialMerge(int[] array, int start, int middle, int end) {//####[98]####
        int[] temp = new int[array.length];//####[99]####
        for (int i = start; i <= end; i++) //####[101]####
        {//####[101]####
            temp[i] = array[i];//####[102]####
        }//####[103]####
        int i = start;//####[105]####
        int j = middle + 1;//####[106]####
        int k = start;//####[107]####
        while (i <= middle && j <= end) //####[110]####
        {//####[110]####
            if (temp[i] <= temp[j]) //####[111]####
            {//####[111]####
                array[k] = temp[i];//####[112]####
                i++;//####[113]####
            } else {//####[114]####
                array[k] = temp[j];//####[115]####
                j++;//####[116]####
            }//####[117]####
            k++;//####[118]####
        }//####[119]####
        while (i <= middle) //####[122]####
        {//####[122]####
            array[k] = temp[i];//####[123]####
            k++;//####[124]####
            i++;//####[125]####
        }//####[126]####
    }//####[127]####
}//####[127]####
